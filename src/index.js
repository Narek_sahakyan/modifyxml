import './assets/scss/style.scss';
var upload = require('upload-element');
var convert = require('xml-js');



class App {
	constructor(height, width) {
		this.elem = document.querySelector('#file-input');
		this.result = null;
		this.json = null;
		this.convertedJson = null;
		this.newXml = null;
		this.fileName = null;
		this.handleUpload();
		this.convertData();
	}


	handleUpload() {
		let self = this;
		upload(self.elem, { type: 'text' }, function(err, files) {
			files.forEach(function(file) {
				self.fileName = file.file.name;
				self.result = file.target.result;
				self.convertXmltojson(self.result);
				document.getElementById("convertBtn").disabled = false
			});
		});
	}


	convertXmltojson (xml) {
		let self = this;
		self.json = convert.xml2json(xml, {compact: true, spaces: 4});
	}

	convertJsonToXml (json) {
		if (typeof (json) === 'object') {
			json = JSON.stringify(json)
		}
		self.newXml = convert.json2xml(json, {compact: true, spaces: 4});
		return self.newXml;
	}

	convertNumberFormatsInJson (data, cb) {
		data = JSON.parse(data);
		var items = data.ExportedData.SignedData;

		items.forEach(function(item){
			var goods = item.Data.SignableData.GoodsInfo.Good;
			if (goods) {
				if (goods.length) {
					goods.forEach(function(good){
						good.Amount._text = good.Amount._text.replace('.', ',')
						good.Price._text = good.Price._text.replace('.', ',')
						good.PricePerUnit._text = good.PricePerUnit._text.replace('.', ',')
						good.TotalPrice._text = good.TotalPrice._text.replace('.', ',')
						good.VAT._text = good.VAT._text.replace('.', ',')
						good.VATRate._text = good.VATRate._text.replace('.', ',')
					});
				} else {
					goods.Amount._text = goods.Amount._text.replace('.', ',')
					goods.Price._text = goods.Price._text.replace('.', ',')
					goods.PricePerUnit._text = goods.PricePerUnit._text.replace('.', ',')
					goods.TotalPrice._text = goods.TotalPrice._text.replace('.', ',')
					goods.VAT._text = goods.VAT._text.replace('.', ',')
					goods.VATRate._text = goods.VATRate._text.replace('.', ',')
				}
			}

		});
		return data;
	}

	saveTextAsFile(fileName, content) {
		var textFileAsBlob = new Blob([content], {type:'text/plain'});
		var fileNameToSaveAs = fileName

		var downloadLink = document.createElement("a");
		downloadLink.download = fileNameToSaveAs;
		downloadLink.innerHTML = "Download File";
		if (window.webkitURL != null) {
			// Chrome allows the link to be clicked
			// without actually adding it to the DOM.
			downloadLink.href = window.webkitURL.createObjectURL(textFileAsBlob);
		} else {
			// Firefox requires the link to be added to the DOM
			// before it can be clicked.
			downloadLink.href = window.URL.createObjectURL(textFileAsBlob);
			downloadLink.onclick = self.destroyClickedElement;
			downloadLink.style.display = "none";
			document.body.appendChild(downloadLink);
		}

		downloadLink.click();
	}

	destroyClickedElement(event) {
		document.body.removeChild(event.target);
	}

	convertData () {
		let self = this;
		var button = document.getElementById("convertBtn");
		button.addEventListener("click",function(e){
			var json = self.json;
			self.convertedJson = self.convertNumberFormatsInJson(json);
			self.saveTextAsFile(self.fileName, self.convertJsonToXml(self.convertedJson));
		},false);
	}
}

window.addEventListener('load', () => new App(), false )